<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Model\Player as Player;
use App\Model\PlayerBattingStatus as PlayerBattingStatus;
use App\Model\PlayerPitchingStatus as PlayerPitchingStatus;
use App\Model\PlayerPosition as PlayerPosition;
use App\Enum\Team as Team;
use App\Enum\Position as Position;
use Goutte\Client;

class UpdatePlayerStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_player_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    // 支配下登録選手
    // http://npb.jp/announcement/2017/registered_f.html
    // 打撃成績
    // http://npb.jp/bis/2017/stats/idb1_f.html
    // 投手成績
    // http://npb.jp/bis/2017/stats/idp1_f.html
    // 守備成績
    // http://npb.jp/bis/2017/stats/idf1_f.html

    private $client;
    private static $team_strs;

    public static $position_strs = [
        '【一塁手】' => [Position::first],
        '【二塁手】' => [Position::second],
        '【三塁手】' => [Position::third],
        '【遊撃手】' => [Position::shortstop],
        '【外野手】' => [Position::left, Position::center, Position::right],
        '【捕手】' => [Position::catcher],
        '【投手】' => [Position::pitcher],
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        self::$team_strs = Team::getTeamStrs();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //インスタンス生成
        $this->client = new Client();

        // playersテーブル更新
        $this->update_players();

        // player_batting_statusesテーブル更新
        $this->update_player_batting_statuses();

        // player_pitching_statusesテーブル更新
        $this->update_player_pitching_statuses();

        // player_positionsテーブル更新
        $this->update_player_positions();

    }

    static $registered_positions = [
        2 => Player::REGISTERED_POSITION_PITCHER,
        3 => Player::REGISTERED_POSITION_CATCHER,
        4 => Player::REGISTERED_POSITION_INFIELDER,
        5 => Player::REGISTERED_POSITION_OUTFIELDER
    ];

    static $bats = [
        '右' => Player::BATS_RIGHT,
        '左' => Player::BATS_LEFT,
        '左右' => Player::BATS_SWITCH
    ];

    static $throws = [
        '右' => Player::THROWS_RIGHT,
        '左' => Player::THROWS_LEFT,
    ];


    // playersテーブル更新
    private function update_players()
    {
        foreach (self::$team_strs as $team_id => $team_str) {
            $url = 'http://npb.jp/bis/teams/rst_' . $team_str . '.html';
            $crawler = $this->client->request('GET', $url);

            $tr_list = $crawler->filter('table.rosterlisttbl tr')->each(function($tr) use ($team_id) {
                // 'rosterMainHead'文字列か$player配列を返す
                $classname = $tr->attr('class');
                if ($classname === 'rosterMainHead') {
                    return 'rosterMainHead';
                } else if ($classname === 'rosterPlayer') {
                    $player = $tr->filter('td')->each(function($td, $index){
                        return $td->text();
                    });
                    return $player;
                } else if ($classname === 'rosterRetire') {
                    return 'rosterRetire';
                }
            });

            $registerd_position_index = 0;
            foreach ($tr_list as $tr) {

                if ($tr === 'rosterMainHead') {
                    $registerd_position_index++;
                    continue;
                } else if ($tr === 'rosterRetire') {
                    continue;
                }

                if (!array_key_exists($registerd_position_index, self::$registered_positions)) continue;

                $player = $tr;

                if (!isset($player[0], $player[1], $player[5], $player[6])) {
                    return;
                }

                $uniform_number = $player[0];
                $name = mb_convert_kana($player[1], 's'); // 姓名の間は半角スペース
                $throws_str = $player[5];
                $bats_str = $player[6];

                if (!preg_match("/^[0-9]+$/", $uniform_number) || !array_key_exists($throws_str, self::$throws) || !array_key_exists($bats_str, self::$bats) ) {
                    return;
                }

                $player = Player::firstOrNew(['team_id' => $team_id, 'uniform_number' => $uniform_number]);
                $player->name = $name;
                $player->registerd_position = self::$registered_positions[$registerd_position_index];
                $player->bats = self::$bats[$bats_str];
                $player->throws = self::$throws[$throws_str];
                $player->save();

            }

            sleep(5);
        }
    }

    // player_batting_statusesテーブル更新
    private function update_player_batting_statuses()
    {
        foreach (self::$team_strs as $team_id => $team_str) {
            $url = 'http://npb.jp/bis/2017/stats/idb1_' . $team_str . '.html';
            $crawler = $this->client->request('GET', $url);

            $trs = $crawler->filter('table tr.ststats')->each(function($tr) use ($team_id) {
                $player_batting_status = $tr->filter('td')->each(function($td, $index){
                    return $td->text();
                });
                return $player_batting_status;
            });


            foreach ($trs as $tr) {
                $player_name = mb_convert_kana($tr[1], 's');
                $player = Player::where('team_id', $team_id)->where('name', $player_name)->first();

                if ($player === null) {
                    \Log::info('update_player_batting_statuses: ' . $player_name . 'がいません');
                    echo('update_player_batting_statuses: ' . $player_name . 'がいません');
                    continue;
                }

                $player_batting_status = PlayerBattingStatus::firstOrNew(['player_id' => $player->id]);

                $player_batting_status->game = $tr[2];
                $player_batting_status->plate_appearance = $tr[3];
                $player_batting_status->runs = $tr[5];
                $player_batting_status->hits = $tr[6];
                $player_batting_status->twobase = $tr[7];
                $player_batting_status->triple = $tr[8];
                $player_batting_status->homerun = $tr[9];
                $player_batting_status->rbi = $tr[11];
                $player_batting_status->fourball = $tr[16] + $tr[17];
                $player_batting_status->deadball = $tr[18];
                $player_batting_status->strikeout = $tr[19];
                $player_batting_status->sacrifice_bunt = $tr[14];
                $player_batting_status->sacrifice_fly = $tr[15];
                $player_batting_status->stolen_base = $tr[12];
                $player_batting_status->caught_stealing = $tr[13];
                $player_batting_status->doubleplay = $tr[20];

                $obp_denominator = $player_batting_status->plate_appearance - $player_batting_status->sacrifice_bunt;
                $player_batting_status->obp = ($obp_denominator > 0) ?  ($player_batting_status->hits + $player_batting_status->fourball + $player_batting_status->deadball) / ($obp_denominator) : 0;
                $slg_denominator = $player_batting_status->plate_appearance - $player_batting_status->fourball - $player_batting_status->deadball - $player_batting_status->sacrifice_bunt - $player_batting_status->sacrifice_fly;
                $player_batting_status->slg = ($slg_denominator > 0) ? ($player_batting_status->hits + $player_batting_status->twobase + $player_batting_status->triple * 2 + $player_batting_status->homerun * 3) / ($slg_denominator) : 0;
                $player_batting_status->ops = $player_batting_status->obp + $player_batting_status->slg;

                $player_batting_status->save();

            }

            sleep(5);
        }
    }

    // player_pitching_statusesテーブル更新
    private function update_player_pitching_statuses()
    {
        foreach (self::$team_strs as $team_id => $team_str) {
            $url = 'http://npb.jp/bis/2017/stats/idp1_' . $team_str . '.html';
            $crawler = $this->client->request('GET', $url);

            $trs = $crawler->filter('table tr.ststats')->each(function($tr) use ($team_id) {
                $player_pitching_status = $tr->filter('td')->each(function($td, $index){
                    return $td->text();
                });
                return $player_pitching_status;
            });


            foreach ($trs as $tr) {
                $player_name = mb_convert_kana($tr[1], 's');
                $player = Player::where('team_id', $team_id)->where('name', $player_name)->first();

                if ($player === null) {
                    \Log::info('update_player_pitching_statuses: ' . $player_name . 'がいません');
                    echo('update_player_pitching_statuses: ' . $player_name . 'がいません');
                    continue;
                }

                $player_pitching_status = PlayerPitchingStatus::firstOrNew(['player_id' => $player->id]);

                $getout = $tr[13] * 3;
                if (!empty($tr[14])) {
                    $getout += $tr[14];
                }

                $player_pitching_status->game = $tr[2];
                $player_pitching_status->plate_appearance = $tr[12];
                $player_pitching_status->getout = $getout;
                $player_pitching_status->allowed_runs = $tr[23];
                $player_pitching_status->earned_runs = $tr[24];
                $player_pitching_status->allowed_hits = $tr[15];
                $player_pitching_status->allowed_homeruns = $tr[16];
                $player_pitching_status->strikeouts = $tr[20];
                $player_pitching_status->bases_on_balls = $tr[17] + $tr[18];
                $player_pitching_status->hit_by_pitches = $tr[19];
                $player_pitching_status->wins = $tr[3];
                $player_pitching_status->losses = $tr[4];
                $player_pitching_status->complete_games = $tr[8];
                $player_pitching_status->shutouts = $tr[9];
                $player_pitching_status->holds = $tr[6];
                $player_pitching_status->saves = $tr[5];

                $player_pitching_status->save();

            }

            sleep(5);
        }
    }

    private function update_player_positions()
    {
        PlayerPosition::truncate();
        $count = PlayerPosition::count();

        // 初回のみ昨シーズンの守備成績も参照
        if ($count === 0) {
            foreach (self::$team_strs as $team_id => $team_str) {
                $url = 'http://npb.jp/bis/2016/stats/idf1_' . $team_str . '.html';
                $this->get_positions_info($url);
            }
        }

        foreach (self::$team_strs as $team_id => $team_str) {
            $url = 'http://npb.jp/bis/2017/stats/idf1_' . $team_str . '.html';
            $this->get_positions_info($url);
        }
    }

    private function get_positions_info($url)
    {
        $crawler = $this->client->request('GET', $url);

        $trs = $crawler->filter('table tr')->each(function($tr) {
            $classname = $tr->attr('class');
            if ($classname === null) {
                if (count($tr->filter('th.sthdfplayer'))) {
                    return $tr->filter('th.sthdfplayer')->eq(0)->text();
                }
            } else if ($classname === 'ststats') {
                if (count($tr->filter('td.stplayer'))) {
                    return mb_convert_kana($tr->filter('td.stplayer')->eq(0)->text(), 's');
                }
            }
            return;
        });

        $position_arr = null;
        foreach ($trs as $tr) {
            if (array_key_exists($tr, self::$position_strs)) {
                $position_arr = self::$position_strs[$tr];
            } else if ($tr !== null && $position_arr !== null) {
                $player = Player::where('name', $tr)->first();
                if (empty($player)) continue;

                foreach ($position_arr as $position) {
                    if (PlayerPosition::where('player_id', $player->id)->where('position', $position)->count() === 0) {
                        $player_position = new PlayerPosition();
                        $player_position->player_id = $player->id;
                        $player_position->position = $position;
                        $player_position->save();
                    }
                }
            }
        }
    }

}
