<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Enum\Team as Team;
use App\Enum\Position as Position;
use App\Enum\Role as Role;
use App\Model\Player as Player;
use App\Model\TeamStartingOrder as TeamStartingOrder;
use Goutte\Client;

class UpdateLatestOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_latest_order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $client;
    private static $team_strs;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        self::$team_strs = Team::getTeamStrs();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->client = new Client();
            $game_counts = $this->getGameCount();
            
            // http://npb.jp/games/2017/
            foreach (self::$team_strs as $team_id => $team_str) {
                $latest_game_url = null;
                $i = 0;

                while ($latest_game_url === null) {
                    // そのチームの最近の１試合のURLを取得
                    $month = str_pad(date('m', strtotime('-' . $i . ' month')), 2, 0, STR_PAD_LEFT);
                    $url = 'http://npb.jp/games/2017/schedule_' . $month . '_detail.html';
                    $crawler = $this->client->request('GET', $url);
                    $tmp_gameurl_list = $crawler->filter('.summary_table table tr td a')->each(function($a) {
                        return $a->attr('href');
                    });
                    $gameurl_list = collect($tmp_gameurl_list)->filter(function ($url) use ($team_str) {
                        $gamestr = collect(explode('/', $url))->filter(function($p) { return $p !== ''; })->last(); // s-d-14
                        $gamestr_arr = explode('-', $gamestr);
                        return (count($gamestr_arr) === 3 && ($gamestr_arr[0] === $team_str || $gamestr_arr[1] === $team_str));
                    });

                    if (count($gameurl_list) > 0) {
                        $latest_game_url = $gameurl_list->last();
                    } else {
                        // 試合がない場合は前の月を検索するためにカウントを１増やす
                        $i++;
                    }
                }

                // この時点で$latest_game_urlに$team_idの球団の最新の試合のURLが格納されている

                $gamestr_arr = explode('-', collect(explode('/', $latest_game_url))->filter(function($p) { return $p !== ''; })->last()); // s-d-14
                $teamtype = ($gamestr_arr[0] === $team_str) ? 'home' : 'visitor';

                $roster_url = $latest_game_url . 'roster.html';
                $order_url = $latest_game_url . 'box.html';

                // $roster_urlからベンチ入りメンバーの背番号と名前のリストを取得
                $crawler = $this->client->request('GET', $roster_url);
                $tr_filter = ($teamtype === 'visitor') ? '.half_left .roster_section table tr' : '.half_right .roster_section table tr';
                $roster_players = $crawler->filter($tr_filter)->each(function($tr) use ($team_id) {
                    if (count($tr->filter('td')) === 3) {
                        $uniform_number = $tr->filter('td.num')->text();
                        $name = $tr->filter('td a')->text();
                        $player = Player::where('team_id', $team_id)->where('uniform_number', $uniform_number)->first();
                        return ($player->getAttribute('id')) ? [
                            'player_id' => $player->id,
                            'uniform_number' => $uniform_number,
                            'name' => $name
                        ] : null;
                    } else {
                        return null;
                    }
                });
                $roster_players = collect($roster_players)->filter(function($player) { return $player !== null; })->values();

                // 試合日取得
                $date_str = strstr($crawler->filter('div.wrap .game_tit time')->text(), '日', true) . '日';
                $date = \DateTime::createFromFormat('Y年m月d日', $date_str)->format('Y-m-d');
                
                // $order_urlから($roster_playersも使い)スタメン情報を取得
                $crawler = $this->client->request('GET', $order_url);
                $section_eq = ($teamtype === 'visitor') ? 0 : 1;
                if (count($crawler->filter('.contents .wrap')) < 2) continue; // 雨天中止の時の表示
                $order_trs = $crawler->filter('.contents .wrap')->eq(1)->filter('section')->eq($section_eq)->filter('div.table_batter table tbody tr')->each(function($tr) {
                    return [
                            'batting_order' => $tr->filter('td')->eq(0)->text(),
                            'position' => $tr->filter('td')->eq(1)->text(),
                            'name' => $tr->filter('td.player a')->text(),
                    ];
                });
                $order_trs = collect($order_trs)->filter(function($order_tr) {
                    return is_numeric($order_tr['batting_order']);
                });

                $starter_name = $crawler->filter('.contents .wrap')->eq(1)->filter('section')->eq($section_eq)->filter('div.table_pitcher table tbody tr td.player')->text();

                $position_strs = Position::getPositionStrs();
                $starting_order = $order_trs->map(function($order_tr, $index) use ($position_strs, $roster_players, $team_id) {
                    $order_tr['position'] = array_search(mb_substr($order_tr['position'], 1, 1), $position_strs);
                    $name = $order_tr['name'];
                    unset($order_tr['name']);
                    $roster_player = $roster_players->first(function($player) use ($name) { return $player['name'] === $name; });
                    //$order_tr['uniform_number'] = $roster_player['uniform_number'];
                    //$player = Player::where('team_id', $team_id)->where('uniform_number', $order_tr['uniform_number'])->first();
                    $order_tr['player_id'] = $roster_player['player_id'];
                    return $order_tr;
                })->values();
                if ($starting_order->contains(function($order_tr) { return $order_tr['position'] === Position::designated_hitter; })) {
                    // DHの選手がオーダーに含まれている場合
                    $roster_player = $roster_players->first(function($player) use ($starter_name, $team_id) { return $player['name'] === $starter_name; });
                    $order_tr = [
                        'batting_order' => 0,
                        'position' => Position::pitcher,
                        //'name' => $starter_name,
                        //'uniform_number' => $roster_player['uniform_number']
                    ];
                    //$player = Player::where('team_id', $team_id)->where('uniform_number', $roster_player['uniform_number'])->first();
                    $order_tr['player_id'] = $roster_player['player_id'];
                    $starting_order->push($order_tr);
                }

                // スタメンではない選手に役割を割り当てる
                $bench_arr_players = $roster_players->filter(function($player) use ($starting_order) {
                    return !$starting_order->contains('player_id', $player['player_id']);
                });
                $tmp_bench_players = collect([]);
                $bench_player_ops_ranking = collect([]);
                $bench_player_hits_ranking = collect([]);
                $bench_player_save_ranking = collect([]);
                $bench_player_hold_ranking = collect([]);
                // ベンチメンバーの成績集計
                foreach ($bench_arr_players as $bench_player) {
                    $player = Player::with(['player_batting_status', 'player_pitching_status'])->where('id', $bench_player['player_id'])->first();
                    $tmp_bench_players->push($player);
                    if ($player->player_positions->pluck('position')->contains('pitcher') && isset($player->player_pitching_status)) {
                        $bench_player_hold_ranking->push($player->player_pitching_status->holds);
                        $bench_player_save_ranking->push($player->player_pitching_status->saves);
                    } else if (isset($player->player_batting_status)) {
                        if ($player->player_batting_status->hits >= 10) {
                            // 10安打以上している選手のOPS記録
                            $bench_player_ops_ranking->push($player->player_batting_status->ops);
                        }
                        $bench_player_hits_ranking->push($player->player_batting_status->hits);
                    }
                }
                $ops_ranking = $bench_player_ops_ranking->sortByDesc(function($ops) { return $ops; })->values();
                $hits_ranking = $bench_player_hits_ranking->sortByDesc(function($hits) { return $hits; })->values();
                $hold_ranking = $bench_player_hold_ranking->sortByDesc(function($hold) { return $hold; })->values();
                $save_ranking = $bench_player_save_ranking->sortByDesc(function($save) { return $save; })->values();
                // ベンチメンバーの集計された成績を利用して役割決定
                $bench_player_roles = [];
                foreach ($tmp_bench_players as $tmp_bench_player) {
                    if ($tmp_bench_player->player_positions->pluck('position')->contains('pitcher') && isset($tmp_bench_player->player_pitching_status)) {
                        if ($save_ranking[0] > 0 && $save_ranking[0] === $tmp_bench_player->player_pitching_status->saves) {
                            $tmp_bench_player->role = Role::closer;
                        } else if ($hold_ranking[0] > 0 && $hold_ranking[0] === $tmp_bench_player->player_pitching_status->holds) {
                            $tmp_bench_player->role = Role::setup_pitcher;
                        } else if ($hold_ranking[1] > 0 && $hold_ranking[1] === $tmp_bench_player->player_pitching_status->holds) {
                            $tmp_bench_player->role = Role::pre_setup_pitcher;
                        } else if ($tmp_bench_player->player_pitching_status->getout < $tmp_bench_player->player_pitching_status->game * 3) {
                            $tmp_bench_player->role = Role::short_relief;
                        } else {
                            $tmp_bench_player->role = Role::long_relief;
                        }
                    } else if (isset($tmp_bench_player->player_batting_status)){
                        if ($tmp_bench_player->player_batting_status->hits >= 10 && $ops_ranking[0] === $tmp_bench_player->player_batting_status->ops || $hits_ranking[0] === $tmp_bench_player->player_batting_status->hits) {
                            $tmp_bench_player->role = Role::dependable_pinch_hitter;
                        } else if ($tmp_bench_player->player_batting_status->hits >= 10 && $ops_ranking[1] === $tmp_bench_player->player_batting_status->ops || $hits_ranking[1] === $tmp_bench_player->player_batting_status->hits) {
                            $tmp_bench_player->role = Role::second_pinch_hitter;
                        } else if ($tmp_bench_player->player_batting_status->hits >= 10 && $ops_ranking[2] === $tmp_bench_player->player_batting_status->ops || $hits_ranking[2] === $tmp_bench_player->player_batting_status->hits) {
                            $tmp_bench_player->role = Role::third_pinch_hitter;
                        } else if ($tmp_bench_player->player_batting_status->game > $tmp_bench_player->player_batting_status->plate_appearance) {
                            $tmp_bench_player->role = Role::fielder;
                        } else {
                            $tmp_bench_player->role = Role::utility;
                        }
                    }

                    if (isset($tmp_bench_player->role)) $bench_player_roles[] = ['player_id' => $tmp_bench_player->id, 'role' => $tmp_bench_player->role];
                }

                $content = [
                    'date' => $date,
                    'roster_player_ids' => $roster_players->pluck('player_id'),
                    'starting_order' => $starting_order,
                    'bench_player_roles' => $bench_player_roles
                ];
                $game_count = array_values(array_filter($game_counts, function($game_count) use ($team_id) { return $game_count['team_id'] === $team_id; } ));
                $game = $game_count[0]['game'];

                $teamStartingOrder = TeamStartingOrder::firstOrNew(['team_id' => $team_id]);
                $teamStartingOrder->content = collect($content)->toJson(JSON_UNESCAPED_UNICODE);
                $teamStartingOrder->game = $game;
                $teamStartingOrder->save();
            }

        } catch(\Exception $e){
            dd($e);
        }

    } 

    private function getGameCount()
    {
        // 各チームの試合数を取得
        $this->client = new Client();
        $url = 'http://npb.jp/bis/2017/stats/';
        $crawler = $this->client->request('GET', $url);
        $game_counts = $crawler->filter('#stdivstandings table td table.stmenustandings table.standings tr')->each(function($tr) {
            if (count($tr->filter('td.standingsTeam')) > 0) {
                return [
                    'teamname' => str_replace('　', '', $tr->filter('td.standingsTeam')->text()),
                    'game' => $tr->filter('td.standingsGame')->text(),
                ];
            } else {
                return null;
            }
        });
        $game_counts = array_values(array_filter($game_counts, function($game_count) { return $game_count !== null; } ));
        $team_formal_short_names = Team::getTeamFormalShortNames();
        $game_counts = array_map(function($game_count) use ($team_formal_short_names) {
            $game_count['team_id'] = array_search($game_count['teamname'], $team_formal_short_names);
            return $game_count;
        } , $game_counts);
        return $game_counts;
    }
}
