<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use Goutte\Client;
use App\Enum\Team as Team;
use App\Model\Player as Player;
use App\Model\NoticeStarter as NoticeStarter;
use App\Model\TotalGameResult as TotalGameResult;

class GetNoticeStarters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get_notice_starter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $client;
    private static $team_formal_names;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        self::$team_formal_names = Team::getTeamFormalNames();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // http://npb.jp/announcement/starter/
        // から予告先発取得
        $this->client = new Client();
        $url = 'http://npb.jp/announcement/starter/';
        $crawler = $this->client->request('GET', $url);

        $date_str = strstr($crawler->filter('.contents .wrap h4')->text(), '日', true) . '日';
        $game_date = \DateTime::createFromFormat('m月d日', $date_str)->format('Y-m-d');

        $starter_pitcher_url_list = $crawler->filter('div.starting_pit_wrapper_top div.unit div a')->each(function($a) {
            return $a->attr('href');
        });

        $notice_starters = [];

        foreach ($starter_pitcher_url_list as $key => $starter_pitcher_url) {
            $url = 'http://npb.jp' . $starter_pitcher_url;
            $crawler = $this->client->request('GET', $url);
            $team_id = array_search($crawler->filter('#registerdivtitle .registerTeam')->text(), self::$team_formal_names);
            $uniform_number = $crawler->filter('.registerNo')->text();
            $player = Player::where('team_id', $team_id)->where('uniform_number', $uniform_number)->first();

            if ($key % 2 === 0) {
                $notice_starter = [
                    'home_team_id' => $team_id,
                    'home_player_id' => $player->id,
                    'game_date' => $game_date,
                ];
                // homeチームの予告先発投手の場合
            } else {
                // visitorチームの予告先発投手の場合
                $notice_starter['visitor_team_id'] = $team_id;
                $notice_starter['visitor_player_id'] = $player->id;
                $notice_starters[] = $notice_starter;
            }
        }

        DB::table('notice_starters')->truncate();
        DB::table('notice_starters')->insert($notice_starters);

        TotalGameResult::truncate(); // シミュレーション勝ち星数リセット
    }
}
