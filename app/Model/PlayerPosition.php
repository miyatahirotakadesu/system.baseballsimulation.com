<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Enum\Position as Position;

class PlayerPosition extends Model
{
    static $position_strs = [
        '【投手】' => Player::REGISTERED_POSITION_PITCHER,
        '【捕手】' => Player::REGISTERED_POSITION_CATCHER,
        '【内野手】' => Player::REGISTERED_POSITION_INFIELDER,
        '【外野手】' => Player::REGISTERED_POSITION_OUTFIELDER,
    ];

    const pitcher = Position::pitcher;
    const catcher = Position::catcher;
    const first = Position::first;
    const second = Position::second;
    const third = Position::third;
    const shortstop = Position::shortstop;
    const left = Position::left;
    const center = Position::center;
    const right = Position::right;

}
