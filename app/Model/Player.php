<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Goutte\Client;

class Player extends Model
{

    protected $fillable = ['team_id', 'uniform_number'];

    const REGISTERED_POSITION_PITCHER = 'P';
    const REGISTERED_POSITION_CATCHER = 'C';
    const REGISTERED_POSITION_INFIELDER = 'I';
    const REGISTERED_POSITION_OUTFIELDER = 'O';

    const BATS_LEFT = 'L';
    const BATS_RIGHT = 'R';
    const BATS_SWITCH = 'S';

    const THROWS_LEFT = 'L';
    const THROWS_RIGHT = 'R';

    public function player_batting_status()
    {
        return $this->hasOne('App\Model\PlayerBattingStatus');
    }

    public function player_pitching_status()
    {
        return $this->hasOne('App\Model\PlayerPitchingStatus');
    }

    public function player_positions()
    {
        return $this->hasMany('App\Model\PlayerPosition');
    }

}
