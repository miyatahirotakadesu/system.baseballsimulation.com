<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlayerBattingStatus extends Model
{
    //
    protected $fillable = ['player_id'];
}
