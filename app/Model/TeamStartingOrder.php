<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TeamStartingOrder extends Model
{
    protected $fillable = ['team_id'];
}
