<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TotalGameResult extends Model
{
    //
    protected $fillable = ['team_id', 'winning_count'];
}
