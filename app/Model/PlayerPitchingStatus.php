<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlayerPitchingStatus extends Model
{
    //
    protected $fillable = ['player_id'];
}
