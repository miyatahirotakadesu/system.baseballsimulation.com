<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Enum\Team as Team;

use App\Model\GameResult as GameResult;
use App\Model\Player as Player;
use App\Model\NoticeStarter as NoticeStarter;
use App\Model\TotalGameResult as TotalGameResult;

use App;
use Twitter;

class GameController extends Controller
{
    // 試合一覧データ取得
    public function stadiuminfo(Request $request)
    {
        //$stadiuminfo = [
        //    ['home' => ['team_id' => 1, 'notice_starter' => '斎藤 佑樹' , 'superiority' => 38], 'visitor' => ['team_id' => 2, 'notice_starter' => '松坂 大輔' , 'superiority' => 62]],
        //    ['home' => ['team_id' => 3, 'notice_starter' => '唐川 侑己' , 'superiority' => 58], 'visitor' => ['team_id' => 4, 'notice_starter' => '菊池 雄星' , 'superiority' => 42]],
        //    ['home' => ['team_id' => 5, 'notice_starter' => '安樂 智大' , 'superiority' => 44], 'visitor' => ['team_id' => 6, 'notice_starter' => '金子 千尋' , 'superiority' => 56]],
        //    ['home' => ['team_id' => 7, 'notice_starter' => '野村 祐輔' , 'superiority' => 71], 'visitor' => ['team_id' => 8, 'notice_starter' => 'マイコラス' , 'superiority' => 29]],
        //    ['home' => ['team_id' => 9, 'notice_starter' => '今永 昇太' , 'superiority' => 52], 'visitor' => ['team_id' => 10, 'notice_starter' => '秋山 拓巳' , 'superiority' => 48]],
        //    ['home' => ['team_id' => 11, 'notice_starter' => '石川 雅規' , 'superiority' => 27], 'visitor' => ['team_id' => 12, 'notice_starter' => '小笠原 慎之介' , 'superiority' => 73]],
        //];
        $notice_starters = NoticeStarter::all();
        $stadiuminfo = $notice_starters->map(function($notice_starter) {
            $home_player = Player::where('id', $notice_starter->home_player_id)->first();
            $visitor_player = Player::where('id', $notice_starter->visitor_player_id)->first();
            return [
                'home' => ['team_id' => $notice_starter->home_team_id, 'notice_starter' => $home_player->name, 'superiority' => 50],
                'visitor' => ['team_id' => $notice_starter->visitor_team_id, 'notice_starter' => $visitor_player->name, 'superiority' => 50],
            ];
        });

        return response()->json(['stadiuminfo' => $stadiuminfo]);
    }

    // 試合一覧データ取得
    public function index(Request $request)
    {
        return response()->json(['game' => []]);
    }

    // 試合詳細データ取得
    public function show(Request $request, $game_id)
    {
        $game_result = GameResult::where('game_id', $game_id)->firstOrFail();
        return response()->json(['game_result' => $game_result->toJson()]);
    }

    // シミュレーション実施
    public function forecast(Request $request)
    {
        /* ユニークなファイル名の一時ファイルを作成 */
        $tmpfname = tempnam("/tmp", "npbsimu");

        $handle = fopen($tmpfname, "w");
        chmod($tmpfname, 0777);
        fwrite($handle, $request->data);
        fclose($handle);

        exec('export LANG=ja_JP.UTF-8;' . env('BASEBALLSIMULATOR_PASS') . ' ' . $tmpfname, $result_json, $result);
        $content = rtrim($result_json[0]);
        $content = json_decode($content, true);

        $request_data = json_decode($request->data, true);

        $notice_starter = NoticeStarter::where('home_team_id', $request_data['home']['id'])->first();
        $content['date'] = $notice_starter->game_date;

        // game_idの規則 = MMDDSS_${home_team_id}_${visitor_team_id}_${マイクロ秒から作られるランダム4文字}
        $game_id = date('mdhis') . '_' . Team::getTeamStrs($request_data['home']['id']) . '_' . Team::getTeamStrs($request_data['visitor']['id']) . '_' . substr(microtime(), -4);
        $game_result = new GameResult();
        $game_result->game_id = $game_id;
        $game_result->content = json_encode($content);
        $game_result->ip_address = $request->ip();
        $game_result->save();

        return response()->json(['game_id' => $game_id]);
    }

    // ユーザーがシミュレーションを行ったあと試合結果画面から送信してくる情報
    public function complete_forecast(Request $request)
    {
        if (!array_key_exists($request->winning_team_id, array_keys(Team::getTeamStrs()))) abort(400);

        $game_result = GameResult::where('game_id', $request->game_id)->firstOrFail();
        if (!empty($game_result->tweeted) || $game_result->ip_address !== $request->ip()) return response();

        // 勝利記録
        $total_game_result = TotalGameResult::firstOrCreate(['team_id' => $request->winning_team_id]);
        $total_game_result->winning_count++;
        $total_game_result->save();

        if (App::environment('production')) {
            $status = $request->tweet_status . ' ' . env('APP_URL') . '/game/' . $game_result->game_id . '/' . $request->view_team_type . ' #プロ野球予報';
            Twitter::postTweet(['status' => $status, 'format' => 'json']);
            $game_result->tweeted = true;
            $game_result->save();
        }

    }

}
