<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Enum\Position as Position;
use App\Model\Player as Player;
use App\Model\NoticeStarter as NoticeStarter;
use App\Model\TeamStartingOrder as TeamStartingOrder;

class TeamController extends Controller
{

    // 前の日の実際の試合のオーダー取得
    public function default_order(Request $request, $team_id)
    {
        $tmp_team_starting_order = TeamStartingOrder::where('team_id', $team_id)->first();
        $team_starting_order = json_decode($tmp_team_starting_order->content, true);

        // 前日の試合のオーダーを使うが、先発投手のところだけ予告先発投手に差し替える
        $notice_starter = NoticeStarter::where('home_team_id', $team_id)->orWhere('visitor_team_id', $team_id)->first();
        $home_team_id = $notice_starter->home_team_id;$home_player_id = $notice_starter->home_player_id;
        $visitor_team_id = $notice_starter->visitor_team_id;$visitor_player_id = $notice_starter->visitor_player_id;
        $starter = Player::where(function($q) use ($team_id, $home_player_id) {
                       $q->where('id', $home_player_id)->where('team_id', $team_id);
                   })->orWhere(function($q) use ($team_id, $visitor_player_id) {
                       $q->where('id', $visitor_player_id)->where('team_id', $team_id);
                   })->first();
        foreach ($team_starting_order['starting_order'] as &$order_item) {
            if ($order_item['position'] === Position::pitcher) {$order_item['player_id'] = $starter['id'];}
        }

        return response()->json($team_starting_order);
    }

    // 両チームの予告先発を取得
    public function notice_starter(Request $request, $team_id)
    {
        $notice_starter = NoticeStarter::where('home_team_id', $team_id)->orWhere('visitor_team_id', $team_id)->first();
        return response()->json($notice_starter);
    }

    public function players(Request $request, $team_id)
    {
        $team_starting_order = TeamStartingOrder::where('team_id', $team_id)->first();
        $game = $team_starting_order->game;

        $players = Player::with(['player_batting_status', 'player_pitching_status', 'player_positions'])->where('team_id', $team_id)->get();
        $players = $players->map(function($player) use ($game) {
            $player->fieldable_positions = $player->player_positions->pluck('position');
            $batting_status_is_pitcher = count(array_values(array_diff($player->fieldable_positions->toArray(), [Position::pitcher, Position::designated_hitter]))) === 0;
            if (isset($player->player_batting_status)) {
                $player->player_batting_status->teams_game = $game;
                $player->player_batting_status->is_pitcher = $batting_status_is_pitcher;
            }
            if (isset($player->player_pitching_status)) $player->player_pitching_status->teams_game = $game;
            return $player;
        });
        return response()->json($players);
    }
}
