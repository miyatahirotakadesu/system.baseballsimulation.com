<?php

namespace App;
use App\Enum\Team as Team;
    
    function getTeamStrs() {
        return [
            Team::FIGHTERS => 'f',
            Team::HAWKS => 'h',
            Team::MARINES => 'm',
            Team::LIONS => 'l',
            Team::EAGLES => 'e',
            Team::BUFFALOES => 'bs',
            Team::CARP => 'c',
            Team::GIANTS => 'g',
            Team::BAYSTARS => 'db',
            Team::TIGERS => 't',
            Team::SWALLOWS => 's',
            Team::DRAGONS => 'd'
        ];
    }

?>
