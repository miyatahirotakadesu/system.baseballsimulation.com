<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class Team extends Enum
{

    const FIGHTERS = 1;
    const HAWKS = 2;
    const MARINES = 3;
    const LIONS = 4;
    const EAGLES = 5;
    const BUFFALOES = 6;
    const CARP = 7;
    const GIANTS = 8;
    const BAYSTARS = 9;
    const TIGERS = 10;
    const SWALLOWS = 11;
    const DRAGONS = 12;

    private static $team_strs = [
        Team::FIGHTERS => 'f',
        Team::HAWKS => 'h',
        Team::MARINES => 'm',
        Team::LIONS => 'l',
        Team::EAGLES => 'e',
        Team::BUFFALOES => 'bs',
        Team::CARP => 'c',
        Team::GIANTS => 'g',
        Team::BAYSTARS => 'db',
        Team::TIGERS => 't',
        Team::SWALLOWS => 's',
        Team::DRAGONS => 'd'
    ];

    private static $team_formal_names = [
        Team::FIGHTERS => '北海道日本ハムファイターズ',
        Team::HAWKS => '福岡ソフトバンクホークス',
        Team::MARINES => '千葉ロッテマリーンズ',
        Team::LIONS => '埼玉西武ライオンズ',
        Team::EAGLES => '東北楽天ゴールデンイーグルス',
        Team::BUFFALOES => 'オリックス・バファローズ',
        Team::CARP => '広島東洋カープ',
        Team::GIANTS => '読売ジャイアンツ',
        Team::BAYSTARS => '横浜DeNAベイスターズ',
        Team::TIGERS => '阪神タイガース',
        Team::SWALLOWS => '東京ヤクルトスワローズ',
        Team::DRAGONS => '中日ドラゴンズ'
    ];

    private static $team_formal_short_names = [
        Team::FIGHTERS => '日本ハム',
        Team::HAWKS => 'ソフトバンク',
        Team::MARINES => 'ロッテ',
        Team::LIONS => '西武',
        Team::EAGLES => '楽天',
        Team::BUFFALOES => 'オリックス',
        Team::CARP => '広島',
        Team::GIANTS => '巨人',
        Team::BAYSTARS => 'DeNA',
        Team::TIGERS => '阪神',
        Team::SWALLOWS => 'ヤクルト',
        Team::DRAGONS => '中日'
    ];


    public static function getTeamStrs($key = null)
    {
        return ($key === null || !array_key_exists($key, self::$team_strs)) ? self::$team_strs : self::$team_strs[$key];
    }

    public static function getTeamFormalNames()
    {
        return self::$team_formal_names;
    }

    public static function getTeamFormalShortNames()
    {
        return self::$team_formal_short_names;
    }

}
