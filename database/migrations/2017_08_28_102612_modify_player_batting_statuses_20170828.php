<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPlayerBattingStatuses20170828 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_batting_statuses', function (Blueprint $table) {
            $table->decimal('obp', 4, 3)->after('doubleplay');
            $table->decimal('slg', 4, 3)->after('obp');
            $table->decimal('ops', 4, 3)->after('slg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_batting_statuses', function (Blueprint $table) {
            $table->dropColumn('obp');
            $table->dropColumn('slg');
            $table->dropColumn('ops');
        });
    }
}
