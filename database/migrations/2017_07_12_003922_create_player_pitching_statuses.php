<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerPitchingStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_pitching_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id');
            $table->integer('game');
            $table->integer('plate_appearance');
            $table->integer('getout');
            $table->integer('allowed_runs');
            $table->integer('earned_runs');
            $table->integer('allowed_hits');
            $table->integer('allowed_homeruns');
            $table->integer('strikeouts');
            $table->integer('bases_on_balls');
            $table->integer('hit_by_pitches');
            $table->integer('wins');
            $table->integer('losses');
            $table->integer('complete_games');
            $table->integer('shutouts');
            $table->integer('holds');
            $table->integer('saves');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_pitching_statuses');
    }
}
