<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyNoticeStarters20170910 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notice_starters', function (Blueprint $table) {
            $table->date('game_date')->after('visitor_player_id')->default('2000-01-01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notice_starters', function (Blueprint $table) {
            $table->dropColumn('game_date');
        });
    }
}
