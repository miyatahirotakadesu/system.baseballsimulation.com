<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyNoticeStarters20170814 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notice_starters', function (Blueprint $table) {
            $table->dropColumn('team_id');
            $table->dropColumn('player_id');
            $table->integer('home_team_id')->after('id');
            $table->integer('home_player_id')->after('home_team_id');
            $table->integer('visitor_team_id')->after('home_player_id');
            $table->integer('visitor_player_id')->after('visitor_team_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notice_starters', function (Blueprint $table) {
            $table->integer('team_id');
            $table->integer('player_id');
            $table->dropColumn('home_team_id');
            $table->dropColumn('home_player_id');
            $table->dropColumn('visitor_team_id');
            $table->dropColumn('visitor_player_id');
        });
    }
}
