<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerBattingStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_batting_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id');
            $table->integer('game');
            $table->integer('plate_appearance');
            $table->integer('runs');
            $table->integer('hits');
            $table->integer('twobase');
            $table->integer('triple');
            $table->integer('homerun');
            $table->integer('rbi');
            $table->integer('fourball');
            $table->integer('deadball');
            $table->integer('strikeout');
            $table->integer('sacrifice_bunt');
            $table->integer('sacrifice_fly');
            $table->integer('stolen_base');
            $table->integer('caught_stealing');
            $table->integer('doubleplay');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_batting_statuses');
    }
}
