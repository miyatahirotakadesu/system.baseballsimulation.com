const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .styles([
       'resources/assets/css/font-awesome.min.css',
       'resources/assets/css/jquery.modal.css',
       'resources/assets/css/layout.css',
       'resources/assets/css/part.css',
       'resources/assets/css/reset.css',
   ], 'public/css/app.css');
