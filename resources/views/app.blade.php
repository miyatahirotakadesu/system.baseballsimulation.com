<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>プロ野球予報</title>
        <meta name="description" content="選手のシーズン成績を元にシミュレーションを行い、今日の試合結果を占います。" />
        <meta name="keywords" content="プロ野球,予報,予想" />

        <link rel="stylesheet" href="{{ mix('css/app.css') }}"></script>

        <script>
            window.Laravel = {
                csrfToken: "{{ csrf_token() }}"
            };
        </script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-106213826-1', 'auto');
          ga('send', 'pageview');
        </script>
    </head>
    <body>
        <div id="app">
            <header class="clearfix">
                <div class="site-branding">
                    <h1 v-on:click="$router.push('/');">プロ野球予報</h1>
                </div>
            </header>
            <div class="container">
                <router-view></router-view>
            </div>
        </div>
    </body>
    <script src="{{ mix('js/app.js') }}"></script>
</html>
