import Vue from 'vue'
import VueRouter from 'vue-router'

require('./bootstrap')

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: require('./components/Index.vue') },
        { path: '/order/:team_id', component: require('./components/Order.vue') },
        { path: '/game/:game_id', component: require('./components/Game.vue') },
        { path: '/game/:game_id/:view_team_type', component: require('./components/Game.vue') },
    ]
})

const app = new Vue({
    router,
    el: '#app'
})
