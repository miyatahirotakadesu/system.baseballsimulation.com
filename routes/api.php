<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/stadiuminfo', 'GameController@stadiuminfo'); // 試合一覧データ取得
Route::get('/notice_starter/{team_id}', 'TeamController@notice_starter'); // 両チームの予告先発を取得
Route::get('/default_order/{team_id}', 'TeamController@default_order'); // 前の日の実際の試合のオーダー取得
Route::get('/players/{team_id}', 'TeamController@players'); // チームの全プレイヤー情報取得
Route::get('/game', 'GameController@index'); // 試合一覧データ取得
Route::get('/game/{game_id}', 'GameController@show'); // 試合詳細データ取得

Route::post('/forecast', 'GameController@forecast'); // シミュレーション実施
Route::post('/complete_forecast', 'GameController@complete_forecast'); // シミュレーション完了後の処理を行う
