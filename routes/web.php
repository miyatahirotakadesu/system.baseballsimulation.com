<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('app'); });
Route::get('/order/{team_class_label}', function () { return view('app'); });
Route::get('/game/{hash}', function () { return view('app'); });
Route::get('/game/{hash}/{view_team_type}', function () { return view('app'); });
